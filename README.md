# Intro_to_Bootstrap_4


In this R file you can have a look at another exercise using bootstrap. You won't need any data because we use birthwt from MASS package. Here are the questions : 

Question 1 : If you have to explain the low variable bsaed on other interesting variables, what kind of statistical problem is it ? What approach can be used ?

Question 2 : If we want to explain bwt variable based on the other interesting variables, what kind of statistical problem is it ? What approach can be used ?

Question 3 : How can you study the association between low and smoke ? 

Question 4 : How can you study the association between bwt and smoke  variables ? 

Question 5 : If we want to study the link between low and smoke, what interest is there to consider the variable race and/or the variable age as explanatory variables in the model ?

Question 6 : Use R to answer the previous questions 

Question 7 : Produce a contingency tab crossing variables low and smoke. Produce a chi-squared test

Question 8 : Remind the null hypothesis in the chi-squared test.

Question 9 : Remind how the p-value is calculated, especially how do we interpret the formulas :

pvalue = PH0(T > t)

pvalue = PH0(|T| > |t|)

What T and t would be like in the chi-squared test realised before ? How the distribution under H0 of the random variable T would be determined ? 

Question 10 : To what correspond the previous option ? 

Question 11 : How do you interpret the previous sentence ? 

Question 12 : Explain what the following sentence means : 

tmp <- .Call(C_chisq_sim, sr, sc, B, E)

Question 13 : To what corresponds STATISTIC ?  What is its link with the chi-squared output ?

Question 14 : Why introduce set.seed ?

Question 15 : What is the utility of sample(low) ? Justify that the contingency matrix is simulated under the independance hypothesis H0 between the variables 

Question 16 : Compare the margins of the tab x_H0 to the tab x. Justify that STATISTIC_H0 corresponds to a test statistic value simulated under J0

Question 17 : By realizing B= 1000 simulations giving each value simulated STATISTIC_H0 and by adapting the code. Deduce the value of PVAL for the critical probability obtained by simulation.

Question 18 : Compare this value to the results obtained when specifying simulate.p.value = TRUE when using chisq.test.

Question 19 : How do we interpret the previous summary ? 

Question 20 : Suggest an approach to calculate the p-value by simulation.

Question 21 : Suggest a bootstrap approach to test the effect of smoke on bwt. We can use a studentized confidence interval approach on the mean differences. Or we can use bootstraped models under H0 

Question 22 : Suggest a linear model to explain bwt according to the variables that you think are interesting. Suggest an approach by bootstrap to obtaine the confidence intervals on the estimated parameters.

Question 23 : Suggest a generalized linear model to explain low according to variables that we think are interesting. Suggest a bootstrap approach to get the confidence intervals on the estimated parameters.


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

November 2021







































